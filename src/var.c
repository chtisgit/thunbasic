#include "tb.h"
#include "lines.h"
#include "var.h"

#include <stdlib.h>
#include <string.h>

static void varset_inccap(struct VarSet *const set)
{
	const int inc = 30;
	void *n = realloc(set->varbuf, sizeof(*set->varbuf) * (set->capacity + inc));
	if(n == NULL)
		outofmemory();
	set->capacity += inc;
	set->varbuf = n;
}

static struct Variable* varset_newvar(struct VarSet *const set, const char *s, char type)
{
	assert(set != NULL && s != NULL);

	if(type == 0) return NULL; /* needed? */

	if(set->len == set->capacity)
		varset_inccap(set);

	assert(set->capacity > set->len);

	struct Variable *v = &set->varbuf[set->len++];
	
	strcpy(v->name, s);
	v->type = type;
	v->dim[0] = 0;
	if(type == '$' || type == '%')
		v->val_i = 0;
	else
		v->val_f = 0;
	
	DEBUGMSG("new var \"%s%c\"\n", s, type);
		
	return v;
}

int varset_has(const struct VarSet *const set, const char *s)
{
	assert(set != NULL);

	int i;
	for(i = 0; i < set->len; i++){
		if(strcmp(s, set->varbuf[i].name) == 0) return 1;
	}
	DEBUGMSG("var %s not found.\n", s);

	if(set->parent != NULL)
		return varset_has(set->parent, s);
	else
		return 0;
}

struct Variable* varset_get_ro(const struct VarSet *const set, const char *s)
{
	int i;
	for(i = 0; i < set->len; i++){
		if(strcmp(s, set->varbuf[i].name) == 0)
			return &set->varbuf[i]; 
	}
	DEBUGMSG("var %s not found.\n", s);

	if(set->parent != NULL)
		return varset_get_ro(set->parent, s);
	else
		return NULL;
}

struct Variable* varset_get(struct VarSet *const set, const char *s, char type)
{
	struct Variable *v = varset_get_ro(set, s);
	
	if(v == NULL)
		return varset_newvar(set, s, type);
	else
		return v;
}

void varset_init(struct VarSet *set, struct VarSet *parent)
{
	set->parent = parent;
	set->capacity = 30;
	set->len = 0;
	set->varbuf = malloc( sizeof(*set->varbuf) * set->capacity );
	if(set->varbuf == NULL)
		outofmemory();
}

void varset_deinit(struct VarSet *set)
{
	assert(set->varbuf != NULL && set->capacity > 0);

	free(set->varbuf);
	set->capacity = 0;
	set->len = 0;
}
