#include "tb.h"
#include "execute.h"

#include <ctype.h>
#include <stdlib.h>
#include <math.h>

static double pm_expression(const char **rd, const struct VarSet *const vs);

static double val_expression(const char **rd, const struct VarSet *const vs)
{
	double val;
	int ch = **rd;

	if(isalpha(ch)){
		char buf[TOKEN_LEN];
		struct Variable *v;

		*rd = gettoken_s(*rd, buf);
		v = varset_get_ro(vs, buf);
		
		if(v != NULL){
			switch(v->type){
			case '#':
				DEBUGMSG("val = %g\n", v->val_f);
				return v->val_f;
			case '$':
				getnum_s(v->val_s, &val);
				DEBUGMSG("val = %g\n", val);
				return val;
			default:
				DEBUGMSG("val = %d\n", v->val_i);
				return v->val_i;
			}
		}else{
			/* ERROR */
			error("variable does not exist");
		}
	}else if(isdigit(ch)){
		*rd = getnum_s(*rd, &val);
		DEBUGMSG("val = %g\n", val);
		return val;
	}else if(ch == '('){
		(*rd)++;
		const double paren = pm_expression(rd, vs);
		if(**rd != ')'){
			error("'(' appears without matching ')'");
		}else{
			(*rd)++;
		}
		return paren;
	}else{
		/* ERROR */
		error("syntax error in expression");
	}
	return 0;
}

static double pow_expression(const char **rd, const struct VarSet *const vs)
{
	double val = val_expression(rd, vs);

	*rd = skipwhite_s(*rd);
	while(**rd == '^'){
		*rd = skipwhite_s(*rd + 1);
		val = pow(val, val_expression(rd, vs));
		*rd = skipwhite_s(*rd);
	}

	return val;
}

static double md_expression(const char **rd, const struct VarSet *const vs)
{
	double val = pow_expression(rd, vs);

	for(;;){
		*rd = skipwhite_s(*rd);
		switch(**rd){
		case '*':
			*rd = skipwhite_s(*rd + 1);
			val *= pow_expression(rd, vs);
			break;
		case '/':
			*rd = skipwhite_s(*rd + 1);
			val /= pow_expression(rd, vs);
			break;
		default:
			DEBUGMSG("md_expression exit char %d\n", **rd);
			goto _ret;
		}
	}
_ret:
	return val;
}

static double pm_expression(const char **rd, const struct VarSet *const vs)
{
	double val = md_expression(rd, vs);

	for(;;){
		*rd = skipwhite_s(*rd);
		switch(**rd){
		case '+':
			*rd = skipwhite_s(*rd + 1);
			val += md_expression(rd, vs);
			break;
		case '-':
			*rd = skipwhite_s(*rd + 1);
			val -= md_expression(rd, vs);
			break;
		default:
			DEBUGMSG("pm_expression exit char %d\n", **rd);
			goto _ret;
		}
	}
_ret:
	return val;
}

int run_let_float(struct VarSet *varset, const char *rd, struct Variable *v)
{
	rd = skipwhite_s(rd);
	v->val_f = pm_expression(&rd, varset);
	return 1;
}

int run_let_int(struct VarSet *varset, const char *rd, struct Variable *v)
{
	rd = skipwhite_s(rd);
	DEBUGMSG("expression parsing: \"%s\"", rd);
	v->val_i = (int) pm_expression(&rd, varset);
	return 1;
}
