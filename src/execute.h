#ifndef _EXECUTE_H_
#define _EXECUTE_H_

#include "var.h"
#include "lines.h"

char* gettoken_s(const char*, char*);
char* getnum_s(const char*, double*const);
char* getnumd_s(const char*, int*const);
char* skipwhite_s(const char*);
void execute(struct Line*, struct VarSet*);

#endif
