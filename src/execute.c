#include "tb.h"
#include "lines.h"
#include "var.h"
#include "debug.h"
#include "calc.h"

#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

char* gettoken_s(const char *s, char *s2)
{
	const char *r = s;

	if(isalpha(*r)){
		while(isalnum(*r) && (r-s) < TOKEN_LEN-1){
			*s2++ = *r++;
		}
	}

	*s2 = '\0';
	return (char*) r;
}

char* skipwhite_s(const char *s)
{
	while(*s == ' ' || *s == '\t')
		s++;

	return (char*) s;
}

char* getnum_s(const char *s, double * const dst)
{
	*dst = strtod(s, (char**) &s);
	return (char*) s;
}
char* getnumd_s(const char *s, int * const dst)
{
	*dst = strtol(s, (char**) &s, 10);
	return (char*) s;
}

static char* read_variable(struct VarSet *const varset, const char *rd, struct Variable **const vdest)
{
	char buf[TOKEN_LEN], type;

	rd = gettoken_s(rd, buf);

	if(buf[0] == '\0'){
		error("variable name expected");
		*vdest = NULL;
		return (char*) rd;
	}
	if(*rd == '$' || *rd == '%')
		type = *rd++;
	else
		type = '#';

	*vdest = varset_get(varset, buf, type);

	return (char*) rd;
}

/* returns -1 on error */
static char* getstring(struct VarSet *const varset, const char *rd, int *const len, char **const ptr)
{
	struct Variable *v;
	int i;

	if(*rd == '\"'){
		i = 0;
		*ptr = (char*) ++rd;
		while(*rd++ != '\"')
			i++;
		*len = i;

	}else if(isalpha(*rd)){
		rd = read_variable(varset, rd, &v);
		*len = strlen(v->val_s);
		*ptr = v->val_s;
	}else{
		error("variable or string literal expected");
	}

	return (char*) rd;
}

/* returns 0 on error */
static int run_let_string(struct VarSet *varset, const char *rd, struct Variable *v)
{
	char *str, *strt;
	int len,n;

	rd = getstring(varset, rd, &len, &strt);
	str = malloc(len+1);
	if(str == NULL)
		outofmemory();

	strncpy(str, strt, len);
	str[len] = '\0';
	rd = skipwhite_s(rd);
	
	while(*rd == '+'){
		rd = skipwhite_s(rd+1);

		rd = getstring(varset, rd, &n, &strt);
		str = realloc(str, len += n);
		strncat(str, strt, n);
		rd = skipwhite_s(rd);
	}

	if(v->val_s != NULL)
		free(v->val_s);
	v->val_s = str;

	return 1;
}

/* return <>0 on error */
int run_let(const struct Line * const l, struct VarSet *varset)
{
	struct Variable *v;
	char *rd = l->rest;
	
	rd = skipwhite_s(rd);

	rd = read_variable(varset, rd, &v);

	if(v == NULL){
		error("variable error");
		return 1;
	}

	rd = skipwhite_s(rd);
	if(*rd++ != '='){
		error("'=' expected");
		return 1;
	}
	rd = skipwhite_s(rd);

	/* and now expression parsing */
	switch(v->type){
	case '$':
		run_let_string(varset, rd, v);
		break;
	case '%':
		run_let_int(varset, rd, v);
		break;
	case '#':
		run_let_float(varset, rd, v);
		break;
	default:
		assert(0);
	}

	return 0;
}

static int run_print(const struct Line * const l, struct VarSet *varset)
{
	char buf[TB_LINE_MAX], *rd = l->rest;
	struct Variable *v;
	int bufi;
	
	for(;;){
		rd = skipwhite_s(rd);
	
		if(*rd == '\"'){	/* String Literal */
			
			rd++;
			for(bufi = 0; *rd != '\"';)
				buf[bufi++] = *rd++;
			rd++;	
			buf[bufi] = '\0';
			printf("%s ", buf);
			
		}else if(isalpha(*rd)){ /* Variable */
			
			rd = gettoken_s(rd, buf);
			
			bufi = '#';
			if(*rd == '%' || *rd == '$')
				bufi = *rd++;
			
			v = varset_get(varset, buf, (char) bufi);
			
			if(v == NULL){
				error("variable not found!");
			}else{
				switch(v->type){
				case '%':
					printf("%d ", v->val_i);
					break;
				case '$':
					printf("%s ", v->val_s);
					break;
				default:
					printf("%g ", v->val_f);
				}
			}
		}else{
			error("jaksldfjlkdjf");
		}
		
		
		rd = skipwhite_s(rd);
		if(*rd++ != ',') break;
		
		
	}
	
	printf("\n");
	
	return 0;
}



void execute(struct Line *line1, struct VarSet *parent_set)
{
	struct VarSet varset;
	struct Line *ip, *next;
	int err = 0;

	varset_init(&varset, parent_set);
	
	for(ip = line1; ip != NULL && err == 0; ip = next){
		next = ip->then;
		
		DEBUGMSG("CMD=\"%s\"\n", BASIC_pcmds_s[ip->cmd]);
		
		switch(ip->cmd){
		case BASCMD_LET:
			err = run_let(ip, &varset);
			break;
		case BASCMD_DIM:
			/* TODO */
			break;
		case BASCMD_PRINT:
			err = run_print(ip, &varset);
			break;
		case BASCMD_INPUT:
			/* TODO */
			break;
		case BASCMD_READ:
			/* TODO */
			break;
		case BASCMD_IF:
			/* TODO */
			break;
		case BASCMD_ON:
			/* TODO */
			break;
		case BASCMD_FOR:
			/* TODO */
			break;
		case BASCMD_NEXT:
			/* TODO */
			break;
		case BASCMD_REM:
		case BASCMD_DATA:
			/* these don't do anything */
		case BASCMD_GOTO:
		case BASCMD_END:
			/* these are run implicitly */
			break;
		case BASCMD_GOSUB:
			/* TODO */
			break;
		case BASCMD_RETURN:
			/* TODO */
			break;
		case BASCMD_DEBUG:
			err = run_debug(ip, &varset);
			break;
		default:
			error("invalid command (this should not happen)");
			err = 1;
		}
	}

	varset_deinit(&varset);
}
