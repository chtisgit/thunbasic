#ifndef _COMMAND_H_
#define _COMMAND_H_

#define CODELINE_FMT	"%-5d %-7s %s\n"
void parse_BASIC_command(void);
void command_run();

#endif

