#include "tb.h"
#include "lines.h"
#include "command.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

/* keep corresponding to enum in tb.h */
const char *BASIC_pcmds_s[] =
	{	"LET", "DIM", "PRINT", "INPUT",
		"DATA", "READ", "IF", "ON", "FOR", 
		"NEXT", "GOTO", "END", "GOSUB",
		"RETURN", "REM", "DEBUG"
	};
const int BASIC_pcmds_len = sizeof(BASIC_pcmds_s)/sizeof(char**);

FILE *lookfile;
char look;
int line;

void error(const char *s)
{
	printf("! error: %s\n", s);
}

void outofmemory(void)
{
	error("out of memory! aborting...");
	exit(1);
}

void errorl(const char *s, int line)
{
	printf("! error in line %d: %s\n", line, s);
}

void getlook(void)
{
	look = fgetc(lookfile);
	if(look >= 'a' && look <= 'z')
		look = toupper(look);
}
void skipwhite(void)
{
	while(look == ' ' || look == '\t')
		getlook();
}
char* getstring(void)
{
	char *buf;
	int maxlen = 50, len = 0;
	
	buf = malloc(maxlen);
	if(buf == NULL)
		outofmemory();

	while(look != '\r' && look != '\n'){
		if(len == maxlen){
			buf = realloc(buf, maxlen += 50);
			if(buf == NULL)
				outofmemory();
			
		}
		buf[len++] = look;
		getlook();
	}
	
	return buf;
}
double getnum(void)
{
	int x = 0, i;
	if(!isdigit(look) && look != '.'){
		error("number expected");
		return 0;
	}
	while(isdigit(look)){
		x = 10*x + look - '0';
		getlook();
	}
	if(look == '.'){
		getlook();
		for(i = 10; isdigit(look); i *= 10){
			x = x + (look - '0') / (double) i;
			getlook();
		}
	}
	return x;
}
void gettoken(char *s)
{
	int i = 0;
	if(isalpha(look)){
		for(; i < TOKEN_LEN-1 && isalnum(look); i++){
			s[i] = look;
			getlook();
		}
	}
	s[i] = '\0';
}

void skip_crlf(void)
{
	while(look == '\r' || look == '\n' || look == ' ' || look == '\t')
		getlook();
}
void read_to_eol(void)
{
	while(look != '\r' && look != '\n')
		getlook();
}

FILE* init_args(int argc, char **argv)
{
	if(argc == 2){
		FILE* f = fopen(argv[1], "r");
		if(f != NULL){
			return f;
		}
		error("could not open file");
	}
	return stdin;
}

int main(int argc, char **argv)
{
	int rval = 0;

	lookfile = init_args(argc, argv);

	if(lookfile == stdin){
		printf(APPTITLE " >:O MUHAHAHA. READY. SET. GO!\n");
		printf("LINE NUMBERS BETWEEN %d AND %d!\n",LINENUM_MIN,LINENUM_MAX);
		printf("\n");

		printf(INPUTLINE_START);
		fflush(stdout);

		getlook();

		for(;;){

			skipwhite();

			if(look == EOF)
				break;

			if(isdigit(look)){		/* BASIC program line */
				parse_BASIC_line();
			}else if(isalpha(look)){	/* BASIC command */
				parse_BASIC_command();
			}else{				/* Syntax error */
				read_to_eol();
				error("syntax error");
			}
			printf(INPUTLINE_START);
			fflush(stdout);


			skip_crlf();
		}
	}else{
		getlook();

		for(;;){

			skipwhite();

			if(look == EOF)
				break;

			if(!isdigit(look)){
				error("no line number");
				rval = 1;
				break;
			}

			parse_BASIC_line();
			skip_crlf();
		}
		if(rval == 0){
			command_run();
		}

		fclose(lookfile);
	}

	lineset_free();

	printf("\n");
	
	return rval;
}

