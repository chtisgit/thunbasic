#ifndef _VAR_H_
#define _VAR_H_

struct VarSet{
	struct VarSet *parent;

	struct Variable *varbuf;
	int len, capacity;
};

struct Variable{
	char name[TOKEN_LEN];
	char type;
	int dim[2];
	union{
		char *val_s;
		int val_i, *arr_i;
		double val_f, *arr_f;
	};
};

void varset_init(struct VarSet *const n, struct VarSet* parent);
void varset_deinit(struct VarSet *const n);
int varset_has(const struct VarSet *const, const char* varname);
struct Variable* varset_get_ro(const struct VarSet *const, const char*);
struct Variable* varset_get(struct VarSet *const, const char*, char);

#endif
