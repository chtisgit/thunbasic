#ifndef _LINE_H_
#define _LINE_H_

struct Line{
	int nr;
	int cmd;
	char *rest;
	struct Line *then, *els;
};

struct LineSet{
	int *linenr;
	struct Line *linestruc;
	size_t len, capacity;
};

int lineset_findpos(int nr);
int lineset_size(void);
void lineset_insert(const struct Line *const line);
void lineset_remove(int nr);
void lineset_free(void);
struct Line* lineset_get(int pos);

struct Line* parse_BASIC_programline(int);
int parse_BASIC_line(void);

struct Line* lines_init(void);
int lines_deinit(void);

#endif
