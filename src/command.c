#include "tb.h"
#include "command.h"
#include "lines.h"
#include "execute.h"
#include "var.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static const char helptext_s[] =
"HELP       ...\tEMITS THIS MESSAGE\n"
"LIST       ...\tEMITS SOURCE CODE OF CURRENT PROJECT\n"
"LIST X     ...\tEMITS CODE LINE X\n"
"LIST X:Y   ...\tEMITS CODE FROM LINE X UNTIL LINE Y\n"
"RUN        ...\tEXECUTES CODE\n"
"NEW        ...\tSTARTS A NEW PROJECT\n"
"LOAD FILE  ...\tLOADS A PROJECT FROM FILE\n"
"SAVE FILE  ...\tSAVES CURRENT PROJECT TO FILE\n"
"SYSTEM     ...\tEXIT BASIC\n"
"EXIT       ...\tEXIT BASIC\n"
"\n"
"LINES STARTING WITH A NUMBER ARE CONSIDERED CODE\n";

					
static void command_help()
{	
	printf("\n" APPTITLE " - HELP\n\n%s\n", helptext_s);
}

static void echolinef(FILE *outp, const struct Line* const l)
{
	const char emptystring[] = {0};
	const char *cmd = BASIC_pcmds_s[l->cmd];
	const char *rest = l->rest == NULL ? emptystring : l->rest;
	fprintf(outp, CODELINE_FMT, l->nr, cmd, rest);
}

static void command_list()
{
	int i = 0, j = LINENUM_MAX;
	int startpos, endpos;

	skipwhite();
	if(isdigit(look)){
		i = getnum();
		if(i < LINENUM_MIN)
			i = LINENUM_MIN;

		if(look == ':'){
			getlook();
			j = getnum();
		}else{
			j = LINENUM_MAX-1;
		}

		startpos = lineset_findpos(i);
		endpos = lineset_findpos(j+1);
	}else{
		startpos = 0;
		endpos = lineset_size();
	}

	assert(startpos >= 0 && startpos <= lineset_size());
	assert(endpos >= 0 && startpos <= lineset_size());

	for(i = startpos; i < endpos; i++){
		echolinef(stdout, lineset_get(i));
	}
}


static void command_save()
{
	char buf[FILENAME_LEN];
	FILE *file;
	int i;
	
	skipwhite();
	if(!isalnum(look)){
		error("filename invalid");
		return;
	}
	
	for(i = 0; i < (FILENAME_LEN-FILEEXT_LEN) && !isspace(look); i++){
		buf[i] = look;
		getlook();
	}
	strcpy(&buf[i], BASICFILE_EXT);
	
	file = fopen(buf, "w");
	
	if(file == NULL){
		error("could not open file");
		return;
	}

	for(i = 0; i < lineset_size(); i++){
		const struct Line *const l = lineset_get(i);
		echolinef(file, l);
	}
	
	fclose(file);
		
}

static void command_load()
{
	char buf[FILENAME_LEN];
	int i;
	
	skipwhite();
	if(!isalnum(look)){
		error("filename invalid");
		return;
	}
	
	for(i = 0; i < (FILENAME_LEN-FILEEXT_LEN) && !isspace(look); i++){
		buf[i] = look;
		getlook();
	}
	strcpy(&buf[i], BASICFILE_EXT); 

	lookfile = fopen(buf, "r");
	if(lookfile == NULL){
		error("could not open file");
		goto onerror;
	}
	
	lineset_free();
	
	getlook();
	while(!feof(lookfile) && look != 0){
		skipwhite();
		if(!parse_BASIC_line()){
			lineset_free();
			break;
		}
		skip_crlf();
	}

onerror:
	lookfile = stdin;
	look = '\n';
}

static void command_new()
{
	lineset_free();
}


static void command_system()
{
	printf("\nThanks for using " APPTITLE " :)\n");
	exit(0);
}

void command_run()
{
	struct Line *head;

	if((head = lines_init())){
		execute(head, NULL);
		lines_deinit();
	}
}


/* Resources to parse BASIC commands */
static const char *BASIC_commands_s[] = {
		"LIST", "RUN", "SYSTEM", "EXIT",
		"NEW", "LOAD", "SAVE", "HELP"
		};
static void (*BASIC_commands_f[])() = {
		command_list, command_run, 
		command_system, command_system,
		command_new, command_load,
		command_save, command_help
		};

void parse_BASIC_command(void)
{
	char command[TOKEN_LEN];
	int i;
	
	gettoken(command);
	
	for(i = 0; i < sizeof(BASIC_commands_s)/sizeof(char**); i++){
		if(strcmp(command, BASIC_commands_s[i]) == 0){
			BASIC_commands_f[i]();
			goto _ret;
		}
	}
	error("unknown command");
_ret:	read_to_eol();
}
