#ifndef _TB_H_
#define _TB_H_

#ifdef NDEBUG
#define DEBUG(X) 
#else
#define DEBUG(X)	X
#endif

#define DEBUGMSG(...)	DEBUG(fprintf(stderr, "DEBUG: " __VA_ARGS__))

#include <stdio.h>
#include <limits.h>
#include <assert.h>

/*	#### DEFINES #### */
#define TOKEN_LEN		16
#define TB_LINE_MAX		150
#define APPTITLE		"Darthunder BASIC"
#define INPUTLINE_START		"> "
#define DEBUGLINE_START		"debug> "
#define BASICFILE_EXT		".BAS"
#define	FILEEXT_LEN		(sizeof(BASICFILE_EXT))
#define FILENAME_LEN		(TOKEN_LEN+FILEEXT_LEN)
#define LINENUM_MIN		0
#define LINENUM_MAX		INT_MAX

enum{	BASCMD_LET, BASCMD_DIM, BASCMD_PRINT, BASCMD_INPUT,
	BASCMD_DATA, BASCMD_READ, BASCMD_IF, BASCMD_ON,
	BASCMD_FOR, BASCMD_NEXT, BASCMD_GOTO, BASCMD_END,
	BASCMD_GOSUB, BASCMD_RETURN, BASCMD_REM,
	BASCMD_DEBUG};

extern FILE *lookfile;
extern char look;
extern int line;
extern const char *BASIC_pcmds_s[];
extern const int BASIC_pcmds_len;

void outofmemory(void);
void getlook(void);
void initlook(void);
void gettoken(char*);
void skipwhite(void);
double getnum(void);
void error(const char*);
void errorl(const char*, int);
void read_to_eol(void);
void skip_crlf(void);

/* debug.c */

#endif
