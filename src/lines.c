#include "tb.h"
#include "lines.h"
#include "execute.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#define LINESET_INIT_CAPACITY	100
#define LINESET_MIN_INC		30
#define LINESET_MAX_UNUSED	200

#define min(A,B)	((A) <= (B) ? A : B)
#define max(A,B)	((A) >= (B) ? A : B)


static struct LineSet lineset = {
	.linenr = NULL,
	.linestruc = NULL,
	.len = 0,
	.capacity = 0 };

int lineset_size(void)
{
	return lineset.len;
}

static void lineset_check(int pos)
{
	if(lineset.linenr == NULL){
		lineset.capacity = LINESET_INIT_CAPACITY;
		lineset.linenr = malloc(sizeof(*lineset.linenr) * lineset.capacity);
		lineset.linestruc = malloc(sizeof(*lineset.linestruc) * lineset.capacity);
		lineset.len = 0;
		if(lineset.linenr == NULL || lineset.linestruc == NULL)
			outofmemory();
	}
	if(pos >= lineset.capacity){
		lineset.capacity += max(pos - lineset.capacity, LINESET_MIN_INC);
		lineset.linenr = realloc(lineset.linenr, sizeof(*lineset.linenr) * lineset.capacity);
		lineset.linestruc = realloc(lineset.linestruc, sizeof(*lineset.linestruc) * lineset.capacity);
		if(lineset.linenr == NULL || lineset.linestruc == NULL)
			outofmemory();
	}
}

int lineset_findpos(int nr)
{
	int start = 0, end = lineset.len;
	int p = start;

	if(end == 0) return 0;

	if(lineset.linenr[0] > nr)
		return 0;

	if(lineset.linenr[lineset.len-1] < nr)
		return lineset.len;

	while(start < end){
		p = (end+start)/2;
		if(lineset.linenr[p] > nr)
			end = p;
		else if(lineset.linenr[p] < nr)
			start = p+1;
		else
			start = end = p;
	}

	return lineset.linenr[p] < nr ? p+1 : p;
}

struct Line* lineset_get(int pos)
{
	if(pos < 0 || pos >= lineset.len)
		return NULL;

	return &lineset.linestruc[pos];
}

void lineset_insert(const struct Line *const line)
{
	int pos = lineset_findpos(line->nr);
	
	if(lineset.len > pos && lineset.linenr[pos] == line->nr){
		lineset_check(0);
		struct Line *const linesave = &lineset.linestruc[pos];
		*linesave = *line;

		pos++;
	}else{
		lineset_check(lineset.len+1);

		if(pos < lineset.len){
			memmove(&lineset.linenr[pos+1], &lineset.linenr[pos],
				sizeof(*lineset.linenr) * (lineset.len - pos));

			memmove(&lineset.linestruc[pos+1], &lineset.linestruc[pos],
				sizeof(*lineset.linestruc) * (lineset.len - pos));
		}
		lineset.linestruc[pos] = *line;
		lineset.linenr[pos] = line->nr;
		lineset.len++;
		
	}

}

void lineset_remove(int nr)
{
	if(lineset.len == 0)
		return;

	const int pos = lineset_findpos(nr);
	
	if(lineset.linenr[pos] != nr)
		return;

	lineset.len--;

	if(pos == lineset.len)
		return;

	memmove(&lineset.linenr[pos], &lineset.linenr[pos+1],
		sizeof(*lineset.linenr) * (lineset.len - pos));

	memmove(&lineset.linestruc[pos], &lineset.linestruc[pos+1],
		sizeof(*lineset.linestruc) * (lineset.len - pos));

}

void lineset_free(void)
{
	if(lineset.capacity >= LINESET_MAX_UNUSED){
		lineset.capacity = LINESET_MAX_UNUSED;
		void *n_nr = malloc(sizeof(*lineset.linenr) * lineset.capacity);
		void *n_struc = malloc(sizeof(*lineset.linestruc) * lineset.capacity);
		if(n_nr == NULL || n_struc == NULL){
			if(n_nr != NULL) free(n_nr);
			if(n_struc != NULL) free(n_struc);
		}else{
			free(lineset.linenr);
			free(lineset.linestruc);
			lineset.linenr = n_nr;
			lineset.linestruc = n_struc;
		}
	}
	lineset.len = 0;
}

static struct Line *line1 = NULL;

static int line_has_rest(const struct Line *const l)
{
	return l->rest != NULL && strlen(l->rest) > 0 ? 1 : 0;
}

struct Line* parse_BASIC_programline(int nolinenr)
{
	struct Line *l = malloc( sizeof(struct Line) );
	char command[TOKEN_LEN];
	int i;

	if(l == NULL)
		outofmemory();
	
	if(!nolinenr)
		l->nr = getnum();

		
	skipwhite();
	gettoken(command);
	skipwhite();
	
	if(strcmp(command, "GO") == 0){	
		gettoken(command+2);
		skipwhite();
		if(strcmp(command, "GOTO") != 0){
			error("GO must be followed by TO");
			free(l);
			read_to_eol();
			return NULL;
		}
	}
	
	l->cmd = -1;
	for(i = 0; i < BASIC_pcmds_len; i++){
		if(strcmp(command, BASIC_pcmds_s[i]) == 0)
			l->cmd = i;
	}
	if(l->cmd == -1)
		l->cmd = BASCMD_LET;
	else
		command[0] = '\0';
	
	
	if(look != '\n' && look != '\r'){
		l->rest = malloc(TB_LINE_MAX);
		if(l->rest == NULL)
			outofmemory();

		strcpy(l->rest, command);

		for(i = strlen(l->rest); look != '\n' && look != '\r' && i < TB_LINE_MAX-1; i++){
			l->rest[i] = look;
			getlook();
		}
		l->rest[i] = '\0';
	}else{
		l->rest = NULL;
	}
	
	l->then = NULL;
	
	return l;
}


int parse_BASIC_line(void)
{
	struct Line *l = parse_BASIC_programline(0);
	if(l == NULL)
		return 0;
	
	if(l->cmd == BASCMD_LET && !line_has_rest(l)){
		/* deleteline(l->nr); */
		/* freeline(l); */
		lineset_remove(l->nr);
	}else{
		/* insertline(l); */
		lineset_insert(l);
	}
	read_to_eol();

	return 1;
}

/* ### initialisations for running code ### */

static struct{
	double *buf;
	int maxlen, len;
	int read;
} data_fields;

int read_data(double *const x)
{
	if(data_fields.read == data_fields.len)
		return 0;
	*x = data_fields.buf[data_fields.read++];
	return 1;
}

static void data_addnum(double x)
{
	if(data_fields.len == data_fields.maxlen)
		data_fields.buf = realloc(data_fields.buf, data_fields.maxlen += 50);
	
	data_fields.buf[data_fields.len++] = x;
}

static void parse_data(const char *s)
{
	double x;
	s = skipwhite_s(s);
	for(;isdigit(*s);){
		s = getnum_s(s, &x);
		s = skipwhite_s(s);
		
		data_addnum(x);
		
		if(*s != ',') break;
		s++;
		s = skipwhite_s(s);
	}
}

static int find_for(struct Line *nline, int pos)
{
	char nextvar[TOKEN_LEN];
	char buf[TOKEN_LEN];
	
	gettoken_s(nline->rest, nextvar);
	
	for(pos--; pos >= 0; pos--){
		struct Line *const l = &lineset.linestruc[pos];

		if(l->cmd == BASCMD_FOR){
			gettoken_s(l->rest, buf);
			if(strcmp(nextvar, buf) == 0){
				nline->then = l;
				return 1;
			}
		}
	}
	errorl("NEXT without corresponding FOR", nline->nr);
	return 0;
}

struct Line* lines_init(void)
{
	int pos;
	int tmp;
	
	data_fields.maxlen = 50;
	data_fields.len = data_fields.read = 0;
	data_fields.buf = malloc(data_fields.maxlen * sizeof(double));
	if(data_fields.buf == NULL)
		outofmemory();
	
	line1 = NULL;
	for(pos = 0; pos < lineset.len; pos++){
		struct Line *const l = &lineset.linestruc[pos];
		if(l->cmd != BASCMD_DATA && l->cmd != BASCMD_REM){
			line1 = l;
			break;
		}
	}
	if(line1 == NULL)
		return NULL;

	struct Line *last = NULL;
	for(pos = 0; pos < lineset.len; pos++){
		struct Line *const l = &lineset.linestruc[pos];

		if(last != NULL && last->then == NULL && l->cmd != BASCMD_DATA && l->cmd != BASCMD_REM)
			last->then = l;

		l->then = l->els = NULL;

		if(l->cmd == BASCMD_GOTO){
			/* tmp = LINE OF GOTO JUMP */
			getnumd_s(skipwhite_s(l->rest), &tmp);
			
			const int jpos = lineset_findpos(tmp);
			if(jpos < 0 || jpos >= lineset.len || lineset.linenr[jpos] != tmp){
				errorl("target line number does not exist", l->nr);
				return NULL;
			}
			l->then = &lineset.linestruc[jpos];
		}
		
		if(l->cmd == BASCMD_DATA)
			parse_data(l->rest);
		
		if(l->cmd != BASCMD_DATA && l->cmd != BASCMD_REM)
			last = l;
		
		if(l->cmd == BASCMD_NEXT){
			if(!find_for(l,pos)) return NULL;
		}
	}

	for(pos = 0; pos < lineset.len; pos++){
		struct Line *const l = &lineset.linestruc[pos];
		struct Line *next;

		if(pos < lineset.len-1){
			next = l+1;
		}else{
			next = NULL;
		}
		
		if(l->cmd != BASCMD_END){
			if(l->then == NULL){
				l->then = next;
			}
			if(l->els == NULL){
				l->els = next;
			}
		}else{
			l->then = l->els = NULL;
		}
	}

	if(last != NULL && last->cmd != BASCMD_GOTO)
		last->then = NULL;
	
	return line1;
}

int lines_deinit(void)
{
	free(data_fields.buf);
	
	return 1;
}
