#include "tb.h"
#include "lines.h"
#include "execute.h"
#include "var.h"

#include <string.h>

static void output_var(struct Variable *v)
{
	printf("%s%c = ", v->name, v->type);
	switch(v->type){
	case '$':
		if(v->val_s)
			printf("\"%s\"\n", v->val_s);
		else
			printf("\"\"\n");
		break;
	case '%':
		printf("%d\n", v->val_i);
		break;
	default:
		printf("%g\n", v->val_f);
	}
}

static void debug_set(struct VarSet *const varset, char *s)
{
	char token[TOKEN_LEN],type;
	struct Variable *v;
	
	gettoken_s(s, token);
	
	if(*token == '\0') return;

	switch(*s){
	case '$':
	case '%':
		type = *s;
		break;
	default:
		type = '#';
	}
	
	v = varset_get(varset, token, type);
	
	printf("new value: ");
	fflush(stdout);
	
	skip_crlf();
	skipwhite();
	switch(v->type){
	case '$':
		
		break;
	case '%':
		v->val_i = getnum();
		break;
	default:
		v->val_f = getnum();
	}
}

static void debug_showall(const struct VarSet *const varset, char *s)
{
	int i;
	
	for(i = 0; i < varset->len; i++){
		output_var(&varset->varbuf[i]);
	}
}

void debug_show(const struct VarSet *const varset, char *s)
{
	char token[TOKEN_LEN];
	
	gettoken_s(s, token);
	
	if(*token == '\0') return;
	
	struct Variable *v = varset_get_ro(varset, token);

	if(v != NULL)
		output_var(varset_get_ro(varset, token));
	else
		error("no such variable");
}


int run_debug(const struct Line * const l, struct VarSet *const varset)
{
	char token[TOKEN_LEN], rest[TB_LINE_MAX];
	int i;
	
	if(l->rest)
		printf("\n--- debug stop %s (line %d)---\n", l->rest, l->nr);
	else
		printf("\n--- debug stop (line %d) ---\n", l->nr);
		
	printf(DEBUGLINE_START);
	fflush(stdout);
	getlook();
	for(;;){
		
		skipwhite();
		gettoken(token);
		
		skipwhite();
		for(i = 0; i < TB_LINE_MAX && look != '\n' && look != '\r'; i++){
			rest[i] = look;
			getlook();
		}
		rest[i] = '\0';
		
		if(strcmp(token, "RESUME") == 0 ||
			strcmp(token, "EXIT") == 0) break;
		
		if(strcmp(token, "SHOW") == 0)
			debug_show(varset, rest);
		else if(strcmp(token, "SHOWALL") == 0)
			debug_showall(varset, rest);
		else if(strcmp(token, "SET") == 0)
			debug_set(varset, rest);
		else
			error("command does not exist - you are in debug mode!");

		printf(DEBUGLINE_START);
		fflush(stdout);
		skip_crlf();
	}
	
	printf("--- debug resume ---\n\n");
	return 0;
}
