#ifndef _CALC_H_
#define _CALC_H_

#include "var.h"

int run_let_int(struct VarSet *varset, const char *rd, struct Variable *v);
int run_let_float(struct VarSet *varset, const char *rd, struct Variable *v);


#endif

